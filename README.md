# Mapas conceptuales "Programación declarativa y funcional"
## *Programacion Declarativa*
```plantuml
@startmindmap
title Cuadro Sinóptico\n**"Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)"**
footer
Elaborado por: **Martinez Jimenez Jennifer**
endfooter
<style>
mindmapDiagram{
    BackGroundColor  	#D3D3D3
}
 node {
  Padding 10
  Margin 8
  HorizontalAlignment center
  LineColor #black
  LineThickness 1.5
  BackgroundColor #SKYBLUE
  RoundCorner 50
  MaximumWidth 300
 }
 arrow{
  LineColor #DEEPPINK
 }
 title{
  FontSize 20
 }
 footer{
  FontSize 18
 }
</style>

* **Programación Declarativa**
**_ surge
*** Como reacción a algunos problemas\nque lleva consigo la:\n -Programación clásica\n -Programación imperativa
**_ objetivo
*** Liberar de las asignaciones a los programadores.
****_ Ventaja
***** Modifica el estado de la memoria\ndel ordenador y además se modifica a\ndetalle paso por paso.
******_ por lo que
******* Evita el cuello de botella 
*** Utilizar recursos específicos que permitan\na los programas un nivel más alto y más\ncercano a la forma de pensar del programador.
** Busca alternativas
***_ según
**** Tipo de programación
*****_ son
****** Programación Funcional
*******_ recurre
******** Lenguaje que ocupan los matemáticos\nLenguaje que describen funciones
******** Razonamiento ecuacional
*******_ ventajas
******** Funciones de orden superior
******** Evaluación perezosa 
*********_ consiste en
********** Computar lo estrictamente necesario
****** Programación Lógica  
*******_ recurre
******** Lógica de predicados
*********_ son
********** Relaciones entre objetos
***********_ estos
************ No establecen datos de:\n-Entrada\n-Salida
*******_ ¿Como funciona?
******** El intérprete dado un conjunto de relaciones y predicados
********* Opera mediante algoritmos
**********_ intenta
*********** Demostrar predicados dentro del sistema
******** El compilador
********* Actúa como motor de inferencia
*******_ ventaja
******** Permite ser más declarativos
** Materias a estudiar        
***_ IA
**** Lenguajes
*****_ PROLOG
****** Lenguaje estándar\nde la programación Lógica
*******_ estudia
******* Conceptos básicos de\nla programación declarativa
*****_ LIPS
****** Lenguaje hibrido
*******_ permite
******** Programación imperativa
***_ Programación\nFuncional Pura
**** Lenguaje estándar 
*****_ HASKELL 
** Libros Guía
*** Programación Funcional
****_ autor
***** **JEROEN FOLKKER**
*** Lenguajes de programación\nconceptos y constructores
****_ autor 
***** **RAVI SETHI**
@endmindmap
```
Informacion tomada de: 
[Programación Declarativa. Orientaciones](https://canal.uned.es/video/5a6f2c66b1111f54718b4911), [Programación Declarativa. Orientaciones y pautas para el estudio](https://canal.uned.es/video/5a6f2c5bb1111f54718b488b) y [Programación Declarativa. Orientaciones](https://canal.uned.es/video/5a6f2c42b1111f54718b4757)


## *Programacion Funcional*
```plantuml
@startmindmap
title Cuadro Sinóptico\n**"Lenguaje de Programación Funcional (2015)"**
footer
Elaborado por: **Martinez Jimenez Jennifer**
endfooter
<style>
mindmapDiagram{
    BackGroundColor  	#FFB6C1
}
 node {
  Padding 10
  Margin 8
  HorizontalAlignment center
  LineColor #white
  LineThickness 1.5
  BackgroundColor #708090
  RoundCorner 50
  MaximumWidth 300
 }
 arrow{
  LineColor #DEEPPINK
 }
 title{
  FontSize 20
 }
 footer{
  FontSize 18
 }
</style>

* **Programación Funcional**
**_ es
*** Paradigma de programación
****_ es
***** Modelo de computación
******_ donde
******* Los lenguajes dotan la semántica\na los programas
**_ se basa
*** Modelo de Von Newman
****_ secuencia
***** Deben ser almacenadas\nantes de ejecutarse
****** EJECUCION
******* Serie de instrucciones secuenciales
********_ Se pueden\nmodificar
********* -Según el estado\ndel computo
**********_ que es
*********** Zona de memoria
**** Zona de memoria
*****_ se accede
****** Mediante una serie de Variables
*****_ almacena
****** Valor de todas las variables
*******_ pueden ser
******** Variables modificables 
**** Instrucción de asignación
*****_ sirve para
****** modificar valor de\nlas variables
****_ llamado 
***** Modelo imperativo
******_ porque
******* Son ordenes
*** Lamba calculo
**** Desarrollado en 1930 aprox.
*****_ por
****** Church Kleene
****_ pensado
***** Sistema para estudiar
****** Naturaleza de las funciones
****** Naturaleza de la computabilidad
****** Recursividad
****_ obtuvieron 
***** Sistema computacional\nequivalente al modelo\nde Von Newman
**** Haskel 
*****_ desarrollo 
****** Lógica combinatoria
******* Variante de lamba calculo
******** que dio 
********* Base de la programación Funcional
**_ variantes
*** Programación Orientada a Objetos
****_ consiste
***** Objetos que interactúan entre si
*** Programación Lógica
****_ formado
***** Conjunto de secuencias
******_ basado
******* Lamba calculo
**_ concepción
*** Matemáticas
*** Funciones
**_ características
*** Las funciones son ciudadanos de primera clase
*** El orden no es relevante
*** Currificacion
****_ es 
***** Una función con diferentes\nparámetros de entrada 
*** Ventajas
**** Aparece la recursión
**** Depende de los parámetros de entrada
***** Transparencia referencial
*** Desventajas
**** La asignación desaparece
**** No existen bucles 
**** Desaparecen las variables
** Evaluación\nde\nfunciones
*** Impaciente\no\nEstricta
****_ consiste
***** Evaluar funciones\nde adentro hacia afuera
****_ característica
***** Fácil de implementar
*** No estricta
****_ consiste
***** Evaluar funciones\nde afuera hacia adentro
****_ característica
***** Difícil de implementar
****_ se conoce\ncomo
***** Evaluación de cortocircuito\no de McCarthy
***** Evaluación perezosa
****** Evalúa los parámetros\nhasta que son necesitados
****** Usa la memoizacion
*******_ consiste
******** Almacenar el valor de\nuna expresión 
** Libro Guía
*** Teoría de los lenguajes de programación
****_ editorial
***** **RAMÓN ARECES**
  @endmindmap
``` 
 Informacion tomada de:
 [Lenguaje de Programación Funcional](https://canal.uned.es/video/5a6f4bf3b1111f082a8b4708)